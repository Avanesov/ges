Usage: ges [COMMAND] [ARGUMENT(s)] [OPTIONS]
Extract and list the extras archives from a collection of gog.com games.

****COMMANDS****
    refresh               Refresh the database files used by extract, and list.
    extract               Extracts the specified archive type(s).
    list                  Lists the specified archive type(s).
    help                  This help document.

****Arguments****
    wallpaper             *wallpaper*.zip       Used with extract and list for this archive type.
    avatar                *avatar*.zip          Used with extract and list for this archive type.
    manual                *manual*.zip          Used with extract and list for this archive type.
    map                   *map*.zip             Used with extract and list for this archive type.
    artwork               *artwork*.zip         Used with extract and list for this archive type.
    reference_card        *reference_card*.zip  Used with extract and list for this archive type.
    book                  *book*.zip            Used with extract and list for this archive type.
    soundtrack            *soundtrack*.zip      Used with extract and list for this archive type.
    all                   Extract will loop through and act on all the above arguments.
                          List will list all .zip archives in the collection.
    managed               Used with list. Lists all managed archives.
    unmanaged             Used with list. Lists all archives that are not currently recognized by ges.

****Options****
    -d|--destination    Sets the write destination for the command in use.
    -a|--archive_source Sets the root read path for gog game collection.
    -c|--cache_source   Sets the root read path for the "database" cache.
    -h|--help           This help document.

****Notes****
*ges assumes that your gog games have all the game content in the root folder for the game.
        No subfolders within the game folder are allowed. If extract operates on an archive
        within a subfolder of the game the logic will place the extracted contents into
        misnamed folders. it expects: root_collection_folder/game_folder/all_game_contents.
*The three options that accept a path should not have a trailing slash, and the path needs to be absolute.
*Log file is located at $HOME/.var/ges/ges.log
*ges uses basename, dirname, find, GNU getopt, and date.
